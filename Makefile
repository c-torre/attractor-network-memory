.POSIX:

.DEFAULT_GOAL = help

help:
	@echo "Check README or Makefile for use details"
clean:
	rm -rf logs/*
	rm -rf figures/*
run:
	python main.py
setup:
	mkdir -p figures
	mkdir -p logs
	pip install -r requirements.txt
all: setup clean run
dev:
	python -X dev main.py
pdb:
	python -m pdb main.py
docker:
	docker-compose run devel bash


.PHONY: all dev docker init pdb run setup start
