"""
Copyright (C) <2021>  <de la Torre-Ortiz C>
GPLv3+. See the 'LICENSE' file for details.

Mean squared error for prediction error.
"""

import numpy as np
from sklearn.metrics import mean_squared_error as mse


def mean_squared_error_min_inv(ground_truth: np.ndarray, prediction: np.ndarray):
    """
    Calculates the `mean squared error <https://en.wikipedia.org/wiki/Mean_squared_error>`_.

    .. math:: \text{MSE} = \frac{1}{N} \sum_{i=1}^N \left(y_{i} - x_{i} \right)^2

    where :math:`y_{i}` is the prediction tensor and :math:`x_{i}` is ground true tensor.

    - ``update`` must receive output of the form ``(y_pred, y)`` or ``{'y_pred': y_pred, 'y': y}``.

    Also does it for the inverse pattern since some computations from patterns to weights
    give the same weights for both the original and inverted pattern.
    """

    g_t = ground_truth
    pred = prediction

    loss = mse(g_t, pred)
    loss_inv = mse(g_t, 1 - pred)
    return min((loss, loss_inv))

def mean_squared_error(ground_truth: np.ndarray, prediction: np.ndarray):
    return mse(ground_truth, prediction)
