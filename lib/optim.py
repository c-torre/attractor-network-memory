"""
    attractor-network-memory.optim
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Optimizers.

    :copyright: (c) 2021 by Carlos de la Torre-Ortiz.
    :license: GPLv3, see LICENSE for more details.
"""


import numpy as np
import torch
from torch.optim import Optimizer
from tqdm import tqdm


def scaled_sigmoid(values, factor=10):
    """Scale the sigmoid.

    Parameters
    ----------
    values : torch.Tensor

    Returns
    -------
    torch.Tensor
    """

    return torch.sigmoid(values) * factor - factor / 2


class Learning(Optimizer):
    """Optimizer to make weight values closer to the correct ones."""

    def __init__(self, params, hyperparams, attractors):
        self._hyperparams = hyperparams
        l_r = self._hyperparams.rate_learning
        # defaults = dict(l_r=l_r, attractors=attractors)
        defaults = dict(l_r=l_r)
        # super(Learning, self).__init__(params, defaults)
        super().__init__(params, defaults)

    def __setstate__(self, state):
        super(Learning, self).__setstate__(state)

    @torch.no_grad()
    def step(self, pattern, closure=None):
        """Performs a single optimization step."""

        loss = None
        if closure is not None:
            with torch.enable_grad():
                loss = closure()

        for idx, group in enumerate(self.param_groups):
            l_r = group["l_r"]
            # attrac = group["attractors"]
            name = group["name"]

            for param in group["params"]:
                # Update model weights for pattern.
                if name == "bag_neur.weight":
                    assert idx == 0
                    # l_vals = attrac.patterns[n_item].detach().clone() * l_r
                    if isinstance(pattern, torch.Tensor):
                        paat = pattern.detach().clone()
                    else:
                        paat = pattern
                    l_vals = paat * l_r
                    param += l_vals
                    continue
                # Update resistant positions.
                if name == "bag_res.weight":
                    assert idx == 1
                    param += l_vals
                else:
                    print("WARNING: Forgetting did not update any tensors.")

        return loss


class Forgetting(Optimizer):
    """Optimizer to make weight values closer to the correct ones."""

    # def __init__(self, params, hyperparams, noise):
    def __init__(self, *, params, hyperparams, random_state=None):

        r_s = random_state
        if isinstance(r_s, np.random._generator.Generator):
            rng = r_s
        elif isinstance(r_s, int):
            rng = np.random.default_rng(r_s)
        else:
            raise TypeError(f"random_state must be int or generator, got {type(r_s)}")

        self.rng = rng

        self._hyperparams = hyperparams
        f_r = self._hyperparams.rate_forgetting
        defaults = dict(f_r=f_r)  # , noise=noise)
        # super(Forgetting, self).__init__(params, defaults)
        super().__init__(params, defaults)

    def __setstate__(self, state):
        super(Forgetting, self).__setstate__(state)

    @torch.no_grad()
    def step(self, n_step, closure=None):
        """Performs a single optimization step."""

        loss = None
        if closure is not None:
            with torch.enable_grad():
                loss = closure()

        for idx, group in enumerate(self.param_groups[::-1]):
            f_r = group["f_r"]
            # noi = group["noise"]
            name = group["name"]

            for param in group["params"]:
                # Use resistant positions to adapt noise.
                if name == "bag_res.weight":
                    assert idx == 0
                    res = param.detach().clone()
                    # gaussian activation of noise on resistant positions.
                    # res = np.e ** (-0.1 * res ** 2)
                    res = np.exp(-0.1 * res**2)
                    noi = self.rng.uniform(-1, 1, size=res.shape)
                    noi_adap = torch.Tensor(noi.copy()) * f_r * res
                    # noi_adap = torch.Tensor(noi[n_step].copy()) * f_r * res
                    continue
                # Update model weights with forgetting.
                if name == "bag_neur.weight":
                    assert idx == 1
                    param += noi_adap
                    # param.apply_(scaled_sigmoid)
                    param = scaled_sigmoid(param)
                else:
                    print("WARNING: Forgetting did not update any tensors.")

        return loss
