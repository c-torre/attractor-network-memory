"""
    attractor-network-memory.ops
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Implemets operators that are specific for Attractor Networks.

    :copyright: (c) 2021 by Carlos de la Torre-Ortiz.
    :license: GPLv3, see LICENSE for more details.
"""

import os
from abc import ABC, abstractmethod

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import torch
from scipy.special import softmax
from sklearn.metrics import mean_squared_error as mse
from tqdm import tqdm

INTS = int, np.int64, np.int32


def mask_bin_arr(bin_arr, proportion=0.5):
    """Take a binary pattern, mask part of it as question to complete.

    Parameters
    ----------
    bin_arr : torch.Tensor
    proportion : float, optional
        1 mask all array, 0 mask none of the array.

    Returns
    -------
    torch.Tensor

    """

    length = len(bin_arr)
    count = int(length * proportion)
    mask = np.hstack((np.zeros(length - count), np.ones(count)))
    if not len(mask) == len(bin_arr):
        raise ValueError("Not same size mask and pattern")
    masked = bin_arr * mask
    return masked


def _str_to_int(str_):
    """Transform a string into its concatenated int representation by encoding
    as UTF8.

    Parameters
    ----------
    str_ : str

    Returns
    -------
    int
    """

    encoded = np.hstack(str_.encode("utf8"))
    joint = np.array2string(encoded, separator="")[1:-1]
    cleaned = joint.replace(" ", "")
    value = int(cleaned)
    return value


def _int_to_bin_arr(item_id, *, size=None):
    """Integer to binary pattern.

    Parameters
    ----------
    item_id : int
    size : int, optional
        Try to fix a pattern size.

    Returns
    -------
    torch.Tensor

    """

    pat_bin = format(item_id, "b")
    pat = np.array(tuple(pat_bin), int)

    l_p = len(pat)
    if size is not None:
        diff = size - l_p
    else:
        diff = 0

    if diff > 0:
        pat = np.pad(pat, (0, diff))
    elif diff < 0:
        msg = f"WARNING: word to pattern size {size} lower than {l_p}. Ignoring given 'size'"
        print(msg)
        raise ValueError(msg)

    pat = torch.Tensor(pat)
    return pat


def encode(item, *, size=None):
    """Encode features as a binary array.
    XXX The strategies are bad

    Parameters
    ----------
    item : TODO

    Returns
    -------
    torch.Tensor

    """

    if isinstance(item, str):
        item = _str_to_int(item)

    if not isinstance(item, INTS):
        raise ValueError(f"{item} is not int and/or could not be converted to int")

    bin_arr = _int_to_bin_arr(item, size=size)

    return bin_arr


class CognitiveNet(ABC):
    """Provides the network-specific cognitive features not related to weight updating."""

    def __init__(self, n_features):
        self._n_features = n_features
        self.working_memory = None

    # @classmethod
    # def item_to_bin_arr(cls, character, n_feat):
    #     """Combine everything to make the pattern from the character.

    #     Parameters
    #     ----------
    #     character : str
    #     n_feat : int

    #     Returns
    #     -------
    #     TODO

    #     """

    #     char_int = cls._str_to_int(character)
    #     bin_arr = cls.encode(char_int, n_feat)

    #     return bin_arr
    @classmethod
    def encode(cls, *args, **kwargs):
        return encode(*args, **kwargs)

    @classmethod
    def _decide(cls, *, pred, replies, n_feat, reply_prec=None):  # , n_pos_replies=6):
        """Get all possible replies and get the most similar to recalled answer."""

        # def decide(cls, n_step, data, pred, n_pos_replies=6):
        # n_rep = n_pos_replies
        # replies = [data.at[n_step, f"pos_reply_{r}"] for r in range(n_rep)]
        # ints = [cls._str_to_int(r) for r in replies]
        # encoded = [cls.encode(i, n_feat) for i in ints]
        is_synth = isinstance(replies[0], np.ndarray)  # synth
        if not is_synth:
            encoded = [cls.encode(r, size=n_feat) for r in replies]
        else:
            encoded = replies
        p_recs = [1 - mse(r, pred) for r in encoded]
        p_recs = softmax(p_recs)

        chosen = replies[np.argmax(p_recs)]
        p_rec = np.amax(p_recs)
        if reply_prec is not None:
            if is_synth:
                idx_rep = np.argmin([mse(r, pred) for r in encoded])
            else:
                (idx_rep,) = np.where(np.array(replies) == reply_prec)
            # chosen = cls.encode(reply_prec, size=n_feat)
            if is_synth:
                p_rec = p_recs[idx_rep]
            else:
                (p_rec,) = p_recs[idx_rep]

        return chosen, p_rec

    def _perceive(self, character, omniscient=False):
        """The learner agent reads the character as a binary pattern.

        Parameters
        ----------
        character : str
        omniscient : bool
            Whether the unmasked pattern will be read.
        """

        # bin_arr = self.item_to_bin_arr(character, self._n_features)
        if not isinstance(character, np.ndarray):  # synth
            bin_arr = encode(character, size=self._n_features)
        else:
            bin_arr = torch.from_numpy(character)

        if not omniscient:
            bin_arr = mask_bin_arr(bin_arr)

        self.working_memory = bin_arr

    @abstractmethod
    def forward(self, bin_arr):
        """Compute forward pass."""

        print(bin_arr)
        raise NotImplementedError

    def reply(self, question, *, pos_replies=None, reply_prec=None):
        """Take a question as pattern and provide a pattern answer.

        Parameters
        ----------
        question : torch.Tensor

        Returns
        -------
        str, float

        """

        # size = self.bag_neur.n_features
        # The problem is here, the kanji can't be directly provided to the forward pass. XXX
        self._perceive(question)
        reply_pattern = self.forward(self.working_memory)

        if self.working_memory is None:
            raise ValueError("Working memory is empty (None)")

        if pos_replies is not None:
            reply, p_recall = self._decide(
                pred=reply_pattern,
                replies=pos_replies,
                n_feat=self._n_features,
                reply_prec=reply_prec,
            )
        else:
            reply, p_recall = reply_pattern, None

        return reply, p_recall


# # -- Flagged not used.
# def _item_to_pattern(item):
#     """Make a bin_arr out of a word.

#     Parameters
#     ----------
#     item : TODO

#     Returns
#     -------
#     torch.Tensor
#     """

#     # item_features
#     bin_arr = None

#     return bin_arr

# def decode(bin_arr):
#     """Take a binary bin_arr and decode it as int."""

#     breakpoint()
#     bin_arr = np.array(bin_arr)
#     item_id = np.array2string(bin_arr, separator="")[1:-1]  # No "[]"
#     item_id = int(item_id, 2)
#     return item_id


# # -- END Flagged not used.
