"""
    attractor-network-memory.agents
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Agents for simulation with different models.

    :copyright: (c) 2021 by Carlos de la Torre-Ortiz.
    :license: GPLv3, see LICENSE for more details.
"""

from abc import ABC, abstractmethod

import numpy as np
import torch

from lib.data import kanji
from lib.ops import mask_bin_arr


def t_step_exists(t_step, data):
    """Check that certain time step exists in dataframe.

    Parameters
    ----------
    t_step : TODO
    data : TODO

    Returns
    -------
    None
    """

    # t_step_in_data = (data["t_step"] == t_step).any()
    # if not t_step_in_data:
    if t_step not in data.index:
        raise ValueError(f"{t_step} t_step not found in data")


class Agent:
    """E.g. a learner, teacher, etc."""

    def __init__(
        self,
        *,
        role,
        id_=None,
        data=None,
        verbose=False,
    ):
        """Agent information.

        Parameters
        ----------
        role = str
            Agent's role in {"learner", "psychologist", "teacher"}
        id_ : str, optional
            Name of the agent. None will take instance ID.
        data : pandas.DataFrame
        verbose : bool
        """

        roles = {"learner", "psychologist", "teacher"}
        if role not in roles:
            raise ValueError(f"Agent role {role} is not in {roles}")
        self.role = role

        self.data = data
        self._verbose = verbose

        self.id_ = self._make_id(id_, data)

        if verbose and data is not None:
            n_steps = len(data)
            print(
                f"[{role}] {self.id_}: received data for simulation (n_steps={n_steps})"
            )

    def _make_id(self, id_, data):
        """Make the agent id if not given."""

        if id_ is None and data is None:
            id_ = str(id(self))
        elif id_ is not None and data is not None:
            id_ = id_
        elif data is not None:
            id_ = data["user"].unique()
            if len(id_) > 1:
                raise ValueError(f"Found > 1 username for one agent:\t{id_}")
            (id_,) = id_
        else:
            print("WARNING: data given, ignoring provided username")
        return id_


class Learner(Agent):

    """An agent that learns items."""

    def __init__(self, *, id_=None, data=None, verbose=False):
        """Learner information.

        Parameters
        ----------

        id_ : str
        data : pandas.DataFrame
        verbose : bool
        """

        Agent.__init__(self, role="learner", id_=id_, data=data, verbose=verbose)

    def learning(self, t_step):
        """Determine whether it is time to learn/review.

        Parameters
        ----------
        t_step : TODO

        Returns
        -------
        bool
            If it's time to learn/review
        """

        if self.data is None:
            # learning = not t_step % t_present - 1.0  # we start at 1
            raise NotImplementedError("Learning without data no bueno")
        else:
            # learning = (self.data["t_step"] == t_step).any()
            learning = t_step in self.data.index

        if self._verbose and learning:
            print(f"I'M LEARNING AT ITER {t_step}!")

        return learning


class Teacher(Agent):
    """FIX MEEE"""

    def __init__(
        self,
        *,
        id_=None,
        n_answers_posible=None,
        minibatch_size=None,
        t_total=None,
        items=None,
        data=None,
        verbose=False,
        question_data=None,
    ):
        """
        Parameters
        ----------
        items
        n_answers_posible : int
        minibatch_size : int
        t_total : int

        """

        Agent.__init__(self, role="teacher", id_=id_, data=data, verbose=verbose)

        # check_positive(n_answers_posible, int)
        # check_positive(minibatch_size, int)
        # check_positive(t_total, int)
        # answers_shape = n_answers_posible, t_total

        # if not answers.shape == answers_shape:
        #     raise ValueError(
        #         f"Answers shape {answers.shape} do not match intended shape {answers_shape}"
        #     )

        # self._t_total = t_total
        # self._n_answers_posible = n_answers_posible
        # self.items = items
        # self.n_items = len(items)
        # self.n_item = 0
        # self.minibatch_size = minibatch_size

        # self.replies = None  # np array

        if not isinstance(data.iloc[0, 0], np.ndarray):  # synth
            self.data = self._select_data(data)
            self.question_data = kanji.get_data()

    @classmethod
    def _select_data(cls, data):
        """Takes user data and selects only information relevant to the teacher.

        Returns
        -------
        None

        """

        # n_replies = 5
        # replies = [f"pos_reply_{n_rep}" for n_rep in range(n_replies)]
        replies = data.columns[data.columns.str.startswith("pos_reply")]
        item = ["item", "item_character", "item_meaning", "item_meaning"]
        metadata = ["success", "teacher_md", "session", "is_eval"]
        cols = [*item, *metadata, *replies]
        data = data[cols]
        return data

    # def _update_item(self):
    #     n_it = self.n_item
    #     n_its = self.n_items
    #     if n_it < n_its - 1:  # XXX
    #         n_it += 1  # XXX
    #     else:  # XXX
    #         # epoch += 1
    #         n_it *= 0  # XXX
    #     # if n_item < n_items - 1:  # XXX
    #     #     n_item += 1  # XXX
    #     # else:  # XXX
    #     #     epoch += 1
    #     #     n_item = 0  # XXX

    # def __next__(self):

    #     prev = self.n_item
    #     self._update_item()
    #     return prev

    @classmethod
    def _get_pos_replies(cls, data, t_step):
        """Give all possible replies for a learning time step.

        Parameters
        ----------
        data : pandas.Series
        t_step : int

        Returns
        -------
        tuple

        """

        t_step_exists(t_step, data)
        pos_replies = data.loc[t_step, data.columns.str.startswith("pos_reply")]
        pos_replies = tuple(pos_replies)

        return pos_replies

    def get_question(self, t_step):
        """Make the appropriate question (partial pattern).

        Parameters
        ----------
        t_step : int

        Returns
        -------
        tuple of str, tuple of str

        """

        data_user = self.data
        if not isinstance(data_user.iloc[0, 0], np.ndarray):  # synth
            data_kanji = self.question_data
        if data_user is not None:
            t_step_exists(t_step, data_user)
            item_id = data_user.at[t_step, "item"]
        else:
            item_id = None

        # character = data_kanji.at[item_id, "character"]
        pos_replies = self._get_pos_replies(data_user, t_step)
        # return character, pos_replies
        return item_id, pos_replies

    def reply(self, t_step):
        """Give the correct reply (string).

        Parameters
        ----------
        t_step : int

        Returns
        -------
        str

        """

        data = self.data
        is_synth = isinstance(data.iloc[0, 0], np.ndarray)  # synth
        if data is not None:
            t_step_exists(t_step, data)
            if not is_synth:
                reply = data.loc[t_step, "item_meaning"][0]  # XXX
                # The [0] needs to be removed when fixing loading data
            else:
                reply = data.loc[t_step, "item"]

        else:
            reply = None

        return reply

    def __call__(self, t_step):
        return self.get_question(t_step)


# def get_conditions(self):
#     """Get the experiment conditions
#     e.g. teacher model, learner model, etc.

#     Returns
#     -------
#     dictionary
#     """

#     conditions = dict(
#         condition=self._condition,
#         teacher_md=self._teacher_md,
#         learner_md=self._learner_md,
#         psy_md=self._psy_md,
#         session=self._session,
#     )
#     return conditions
# def identify(self):
#     """Get user data related to identity
#     e.g. user name, domain
#     """

#     identity_data = dict(
#         user=self.data["user"],
#         domain=self.data["domain"],
#     )
#     return identity_data
