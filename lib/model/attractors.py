"""
Copyright (C) <2021>  <de la Torre-Ortiz C>
GPLv3+. See the 'LICENSE' file for details.

Network weights calculation.
"""

import numpy as np
import torch


class Attractors:
    """Attractors from patterns."""

    def __init__(self, patterns: np.ndarray):
        # self.patterns = torch.empty_like(patterns)
        # self.patterns = self._encode_attractors(patterns)  LAST COMMENTED
        # self.cumsum = torch.empty_like(patterns)
        # (num_neurons, num_neurons, num_patterns)

        # Calls
        # self._encode_attractors(patterns)
        # self._get_cumsum()
        pass

    def __str__(self):
        return f"{self.patterns}"

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        return len(self.patterns)

    def __getitem__(self, item):
        return self.patterns[item]

    @classmethod
    def _get_attractor(cls, pattern: np.ndarray, diag_zeros: bool = True) -> np.ndarray:
        """Make each element in the pattern as Hebb's rule equation."""

        assert pattern.size
        mod_pattern = 2 * pattern - 1  # Each element as in Hebb's rule equation
        attrac = mod_pattern[:, None] @ mod_pattern[None, :]
        if diag_zeros:
            np.fill_diagonal(attrac, 0)

        return attrac

    @classmethod
    def _get_add_attractor(cls, pattern: np.ndarray) -> np.ndarray:
        """Make attractors with an addition instead of product EXPERIMENTAL"""

        assert pattern.size
        attrac = pattern[:, None] + pattern[None, :] - 1
        attrac = torch.Tensor(attrac)

        return attrac

    @classmethod
    def encode(cls, pattern, *, how="add"):
        """Interface the function used to encode attractors."""

        methods = "add", "prod"
        if how == "add":
            return cls._get_add_attractor(pattern)
        if how == "prod":
            return cls._get_attractor(pattern)
        raise ValueError(f"Encoding method not in {methods}")

    def _encode_attractors(self, patterns):
        """Encode all patterns as attractors."""

        patts = patterns
        encode = self.encode("add")

        # self.patterns = torch.stack([encode(p) for p in patts])
        attractors = {it: encode(pat) for it, pat in patts.items()}
        return attractors

    # def _get_cumsum(self):

    #     self.cumsum = np.cumsum(self.patterns, axis=0)
