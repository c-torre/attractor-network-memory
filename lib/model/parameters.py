"""
Copyright (C) <2021>  <de la Torre-Ortiz C>
GPLv3+. See the 'LICENSE' file for details.

Model parameters.
"""

import configparser
import os
from typing import Any

from lib.model.attractors import Attractors
from lib.utils import logger
from numpy.random import default_rng


class Parameters:
    """Network parameters loading and transformations."""

    def __init__(self):

        self._config: configparser.ConfigParser = None
        self._num_parameters: int = None
        self._checks: int = 0

        # Primary
        self.t_continuous: float = None
        self.t_step: float = None
        self.t_presentation: float = None
        self.rate_learning: float = None
        self.rate_forgetting: float = None
        self.gain_exponent: float = None

        # Derived
        self.t_discrete: int = None

        # Calls
        self._init_num_parameters()
        self._get_config()
        self._get_parameters()
        self._final_check()

    def _get_instance_parameters(self) -> dict:
        """Get all public attributes: the parameters."""

        return {
            param: val for param, val in vars(self).items() if not param.startswith("_")
        }

    def __str__(self):
        self._final_check()
        return f"{self._get_instance_parameters()}"

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        self._final_check()
        assert isinstance(self._num_parameters, int)
        return self._num_parameters

    def _init_num_parameters(self) -> None:
        """Get the number of model parameters from public attr count."""

        self._num_parameters = len(self._get_instance_parameters())

    def _get_config(self) -> None:

        conf = "parameters.ini"
        if not conf in os.listdir("."):
            raise FileNotFoundError(f"{conf} is required at project root")
        config = configparser.ConfigParser()
        config.read(conf)

        param = "parameters"
        assert config.has_section(param)
        self._config = config

    def _get_param_float(self, parameter) -> Any:

        return self._config.getfloat("parameters", parameter)

    def update_check(func) -> callable:
        def wrapper(self, *args, **kwargs):
            self._checks += 1
            return func(self, *args, **kwargs)

        return wrapper

    # Get, check, and set parameters with their constrains.

    @update_check
    def _get_t_continuous(self) -> None:

        t_continuous = self._get_param_float("t_continuous")

        assert isinstance(t_continuous, float)
        assert t_continuous >= 0

        self.t_continuous = t_continuous

    @update_check
    def _get_t_step(self) -> None:

        t_step = self._get_param_float("t_step")

        assert isinstance(t_step, float)
        assert t_step >= 0

        self.t_step = t_step

    @update_check
    def _get_t_presentation(self) -> None:

        t_presentation = self._get_param_float("t_presentation")

        assert isinstance(t_presentation, float)
        assert t_presentation > 0

        self.t_presentation = t_presentation

    @update_check
    def _get_gain_exponent(self) -> None:

        gain_exponent = self._get_param_float("gain_exponent")

        assert isinstance(gain_exponent, float)
        assert gain_exponent >= 0

        self.gain_exponent = gain_exponent

    @update_check
    def _get_rate_learning(self) -> None:

        param = self._get_param_float("rate_learning")

        assert isinstance(param, float)
        assert 0 <= param <= 1

        self.rate_learning = param

    @update_check
    def _get_rate_forgetting(self) -> None:

        param = self._get_param_float("rate_forgetting")

        assert isinstance(param, float)
        assert 0 <= param <= 1

        self.rate_forgetting = param

    @update_check
    def _get_t_discrete(self) -> None:

        assert not self.t_continuous % (
            1 / self.t_step
        ), "t_cont / t_step remainder is not 0"

        t_discrete = int(self.t_continuous / self.t_step)

        assert isinstance(t_discrete, int)
        assert t_discrete >= 0

        self.t_discrete = t_discrete

    # END get, set, check

    def _get_parameters(self) -> None:
        self._get_t_continuous()
        self._get_t_step()
        self._get_t_presentation()
        self._get_rate_learning()
        self._get_rate_forgetting()
        self._get_gain_exponent()

        # Derived
        self._get_t_discrete()

    def _final_check(self) -> None:

        parameters = self._get_instance_parameters()
        num_parameters = len(parameters)
        num_checks = self._checks

        assert (
            num_parameters == num_checks
        ), f"{num_parameters} parameters not equal to {num_checks} checks"
        assert all(val is not None for val in parameters.values())


class Hyperparameters:
    def __init__(
        self, n_features, params, patterns, random_state, verbose, time=None, data=None
    ):
        """Contains hyperparameters.

        Parameters
        ----------
        params : Parameters
        patterns : np.ndarray
        random_state : int, default=123
        verbose : bool, default=False
        """

        self.random_state = random_state
        self._rng = default_rng(random_state)
        self._verbose = verbose
        self._n_features = n_features

        self.params = params
        self.patterns = patterns  # (num_patterns, n_features)
        self.attractors = Attractors(self.patterns)
        self.noise = None  # (n_features, n_features, t_total)

        self.time = self._make_time(time, data)
        # self._init_noise()

    # def _init_noise(self):
    #     """Compute the noise for all iterations."""

    #     rng = self._rng
    #     # steps = self.params.t_discrete
    #     steps = self.time
    #     shape = logger.get_mat_shape(steps, self._n_features)  # (int, int, int)
    #     self.noise = rng.uniform(-1, 1, size=shape)

    @staticmethod
    def _make_time(time, data):
        if time is None and data is None:
            raise ValueError("Must give either data to simulate or time")
        if time is not None and data is not None:
            raise ValueError("Cannot give both data and explicit time")
        if time is None and data is not None:
            # time = len(data)
            time = data.index.max()
        return time
