"""
Copyright (C) <2021>  <de la Torre-Ortiz C>
GPLv3+. See the 'LICENSE' file for details.

Attractor network that learns and forgets.
"""

from functools import partial

import numpy as np
import torch
from lib.ops import _int_to_bin_arr


class PatternsGenerator:
    """Make patterns."""

    def __init__(
        self,
        size: int = None,
        *,
        n_patterns: int = None,
        sparsity: int = 0.5,
        random_state: int = 0,
        verbose: bool = False,
    ):

        self._rng = np.random.default_rng(random_state)
        self.n_patterns = n_patterns
        self.size = size
        self.sparsity = sparsity
        self._verbose = verbose

    def __len__(self):
        return self.size

    # @property
    # def size(self):
    #     """Get size."""

    #     assert self._size > 0
    #     return self._size

    # @size.setter
    # def size(self, size):
    #     """Set size if value is valid."""

    #     if isinstance(size, int) and size > 1:
    #         self._size = size
    #     else:
    #         raise ValueError("Patterns size must be int > 1")

    def random_binary(self, size=None, *, n_patterns=None) -> np.ndarray:
        """Make simple pattern array of random integers in {0,1}."""
        rng = self._rng

        if self._verbose:
            print("Making random patterns...")

        sp_probs = self.sparsity, 1 - self.sparsity
        opts = 0, 1

        if self.n_patterns is None and n_patterns is None:
            raise TypeError("Either n_patterns attribute or arg must exist")
        if self.size is None and size is None:
            raise TypeError("Either size attribute or arg must exist")
        n_pats = self.n_patterns if n_patterns is None else n_patterns
        size = self.size if size is None else size

        return rng.choice(opts, size=(n_pats, size), p=sp_probs)

    @staticmethod
    def manual_binary(patterns):
        """Make simple pattern array of binary integers.

        Parameters
        ----------
        patterns : list, tuple, np.ndarray

        Returns
        -------
        np.ndarray : patterns.

        """

        pats = patterns

        if len({len(pat) for pat in pats}) != 1:
            raise ValueError("Patterns must have the same lenght")

        pats = np.vstack(pats)

        opts = {0, 1}
        assert set(np.ravel(pats)).issubset(opts)

        return pats

    def user_fakes(self, item_ids):
        """Transform seen item IDs into their binary representations
        and use those as patterns.
        """

        # next one only works if you take num item as patter
        # rather then word to int
        # self.size = len(format(item_ids.max(), "b"))
        # int2pat = lambda x: _int_to_bin_arr(x, size=self.size)
        int2pat = partial(_int_to_bin_arr, size=self.size)
        pats = {n_item: int2pat(val) for n_item, val in item_ids.iteritems()}
        # pats = torch.stack(pats)
        return pats
