def net_plot(rate_learning, rate_forgetting, t_presentation, seed=0):
    """Instantiate a network for plotting.

    Parameters
    ----------
    rate_learning : int
    rate_forgetting : int
    t_presentation : int
    seed : int = 0

    Returns
    -------
    net : object

    """

    params = Parameters()
    patterns = PatternsGenerator(size=10, num_patterns=1, seed=seed).random_binary()
    net = AttractorNet(params, patterns, verbose=False)

    net.params.rate_learning = rate_learning
    net.params.rate_forgetting = rate_forgetting
    net.params.t_presentation = t_presentation
    net.run()

    return net


@cache
def learner_weights():

    fig, axs = plt.subplots(2, 2, sharex=True, sharey=True)

    # Good learner, good schedule.
    net = net_plot(0.8, 0.3, 5.0)
    t_continuous = net.params.t_continuous
    t_half = int(t_continuous // 2)
    data = net.weights[t_half]
    sns.heatmap(data, cmap="viridis", vmin=-6, vmax=6, ax=axs[0, 0])
    axs[0, 0].set_title("LG, SG")

    # Good learner, bad schedule.
    net = net_plot(0.8, 0.3, 10.0)
    data = net.weights[t_half]
    sns.heatmap(data, cmap="viridis", vmin=-6, vmax=6, ax=axs[0, 1])
    axs[0, 1].set_title("LG, Sb")

    # Bad learner, good schedule.
    net = net_plot(0.4, 0.7, 5.0)
    data = net.weights[t_half]
    sns.heatmap(data, cmap="viridis", vmin=-6, vmax=6, ax=axs[1, 0])
    axs[1, 0].set_title("Lb, SG")

    # Bad learner, bad schedule.
    net = net_plot(0.4, 0.7, 10.0)
    data = net.weights[t_half]
    sns.heatmap(data, cmap="viridis", vmin=-6, vmax=6, ax=axs[1, 1])
    axs[1, 1].set_title("Lb, Sb")

    plt.tight_layout()
    fig.savefig("figures/conditions_sum_clip.pdf")


def plotty(net):
    # fast fix, won't work
    sns.set_theme()
    sns.set_context("paper")
    plt.close("all")
    fig = sns.relplot(
        x="t_step", y="loss", data=net.loss_df, kind="line", hue="n_pattern"
    )
    # fig.set(xticks=range(30))
    fig.savefig("figures/plotty_sum_clip.pdf")


def similarity(pattern_0, pattern_1):

    p_0 = pattern_0
    p_1 = pattern_1

    assert len(p_0) == len(p_1)
    return p_0 @ p_1
