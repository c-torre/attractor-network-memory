"""
Copyright (C) <2021>  <de la Torre-Ortiz C>
GPLv3+. See the 'LICENSE' file for details.

Plotting with a slider.
"""

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import Button, Slider


def imshow_slider(array: np.ndarray):

    data = array

    fig, ax = plt.subplots()
    plt.subplots_adjust(bottom=0.25)
    ax.imshow(data[0])
    ax.margins(x=0)

    ax_slider = plt.axes([0.2, 0.1, 0.65, 0.03])

    slider_time = Slider(ax_slider, "Time", 0, len(data) - 1, valinit=0, valstep=1)

    def update(val):
        freq = slider_time.val
        ax.imshow(data[freq])
        fig.canvas.draw_idle()

    slider_time.on_changed(update)

    button_reset = plt.axes([0.8, 0.025, 0.1, 0.04])
    button = Button(button_reset, "Reset", hovercolor="0.975")

    def reset(event):
        slider_time.reset()

    button.on_clicked(reset)

    # Initialize plot with correct initial active value

    plt.show()
