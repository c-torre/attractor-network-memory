#!/usr/bin/env python
"""
    attractor-network-memory.main
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Attractor network that learns and forgets.

    :copyright: (c) 2021-2022 by Carlos de la Torre-Ortiz.
    :license: GPLv3, see LICENSE for more details.
"""


import datetime
from functools import partial
from itertools import cycle

import matplotlib.pyplot as plt
import numpy as np
import optuna
import pandas as pd
import seaborn as sns
import torch
from sklearn.metrics import mean_squared_error as mse
from torch import nn
from torch.nn.parameter import Parameter
from tqdm import tqdm

from lib.agents import Teacher
from lib.attractor_net import AttractorNet
from lib.data import loader
from lib.model.attractors import Attractors
from lib.model.parameters import Hyperparameters, Parameters
from lib.model.patterns import PatternsGenerator
from lib.ops import CognitiveNet as cgn
from lib.ops import encode
from lib.optim import Forgetting, Learning
from lib.plot.slider import imshow_slider
from lib.utils import _paths
from lib.utils.datetime import TODAY
from lib.utils.logger import History, logger

plt.close("all")


def check_positive(value, type_):
    """Check whether a numeric value is positive."""

    if not isinstance(value, type_):
        raise TypeError(f"{value} is not {type_}")
    if not value > 0:
        raise ValueError(f"{value} is negative")


class Course:
    """Should contain all running results, questions, answers..."""

    def __init__(self):
        self._fields = frozenset(
            {
                "user",
                "domain",
                "condition",
                "item",
                "item_character",
                "item_meaning",
                "success",
                "teacher_md",
                "learner_md",
                "psy_md",
                "session",
                "is_eval",
                "pos_reply_1",
                "pos_reply_2",
                "pos_reply_3",
                "pos_reply_4",
                "pos_reply_5",
                "timestamp",
                "item_id",
            }
        )
        self._reply_default = {field: None for field in self._fields}
        self._results = pd.DataFrame(columns=self._fields)

        self.reply = {field: None for field in self._fields}

    def store_reply(self, idx):
        """Check that current reply information is complete
        and append it to the running results dataframe.
        Set the current reply fields to None again.
        """

        reply = self.reply
        if idx in self._results.index:
            raise ValueError("Index already found in results dataframe")
        if None in reply.values():
            raise ValueError("All reply fields must be filled before storing it")
        if not all(field in self._fields for field in reply.keys()):
            raise ValueError("Fields in current reply do not match fields in results")

        reply = pd.Series(reply, name=idx, dtype=object)
        self._results = self._results.append(reply)
        self.reply = self._reply_default

    @property
    def patterns(self) -> np.ndarray:
        """Get patterns."""

        return self._patterns

    @patterns.setter
    def patterns(self, patterns: np.ndarray):
        """Set patterns if not empty."""

        if patterns.size:
            self._patterns = patterns
        else:
            raise ValueError("Patterns must not be empty")


def main():
    """Main function."""
    pass


# if __name__ == "__main__":
#     main()


def log_likelihood(arr_p):
    """TODO: Docstring for log_likelihood.

    Parameters
    ----------
    arg1 : TODO

    Returns
    -------
    TODO

    """

    eps = np.finfo(float).eps
    arr_p += eps
    arr_log = np.log(arr_p)
    log_l = np.sum(arr_log)

    return log_l


def sim(trial=None, params=None, user=None, data_prec=None, data_user=None):
    if trial is not None and params is not None:
        raise ValueError("Trials and params are both not None")
    verbose = trial is None
    # Reading data.
    if data_user is None:
        data_user = loader.prepare_user_data(user)
        is_synth = False
    else:
        is_synth = True

    parameters = Parameters()

    # Patterns.
    if is_synth:
        n_features = len(data_user.iloc[0, 0])
    else:
        n_features = 130

    # patterns = PatternsGenerator(size=n_features, n_patterns=3, random_state=1).random_binary()
    if is_synth:
        patterns = data_user["item"].drop_duplicates()
    else:
        patterns = PatternsGenerator(
            size=n_features, n_patterns=3, random_state=1
        ).user_fakes(data_user["item"])

    # n_features = patterns[0].size()
    if isinstance(n_features, torch.Size):
        (n_features,) = list(n_features)

    # model = AttractorNet(n_features=n_features)
    # learner = Learner(model, data=data_user, verbose=True)
    kwargs = dict(n_features=n_features, agent_data=data_user, verbose=verbose)
    if is_synth:
        kwargs["agent_id"] = TODAY
    learner = AttractorNet(**kwargs)
    for param in learner.parameters():
        param.requires_grad = False

    # Time
    # time = parameters.t_discrete  # This is a session parameter, not a model param.
    t_present = int(parameters.t_presentation)  # WON'T WORK dt != 1.0 XXX

    # Learning items.
    n_items = len(patterns)  # XXX
    n_item = 0  # First item to present.
    epoch = 0  # For history.
    minibatch_size = 1  # Items per learning session.

    n_items = 3  # XXX to avoid 1412 items for now

    # Hyperparameters.
    session = Hyperparameters(
        n_features,
        parameters,
        patterns,
        random_state=123,
        verbose=verbose,
        data=data_user,
    )
    # history = History(data=data_user, n_features=n_features)
    time = session.time
    # time = data_user.index.max()

    # Optimizers.
    param_update = [
        {"params": tensor, "name": name}
        for name, tensor in dict(learner.named_parameters()).items()
    ]

    # Parameters: fixed or in optimization.
    if trial is not None:
        session.params.rate_learning = trial.suggest_float(
            "rate_learning", 0.0, 1.0  # l_r
        )
        # session.params.rate_forgetting = trial.suggest_float(
        #     "rate_forgetting", 0.0, 1.0  # f_r
        # )
        session.params.rate_forgetting = 0.5  # XXX XXX
    if params is not None:
        session.params.rate_learning = params["rate_learning"]
        session.params.rate_forgetting = params["rate_forgetting"]

    learning = Learning(param_update, session.params, session.attractors)
    forgetting = Forgetting(
        params=param_update, hyperparams=session.params, random_state=0
    )  # session.noise)

    # teacher = Teacher([0, 1, 2], minibatch_size)
    kwargs = dict(data=data_user, verbose=verbose)
    if is_synth:
        kwargs["id_"] = TODAY
    teacher = Teacher(**kwargs)

    loss_fn = nn.MSELoss()

    def simulate(data_prec=None):
        """Will simulate taking from GLOBALS.
        if given some data, it must be the result of a simulation (choice the mode made, success, etc.)
        Then it'll record the p of the chioice recorded in the data with the current model.
        This is for parameter recovery.
        """

        param_rec = data_prec is not None
        # learner, history, time, n_items, n_features, learning, forgetting, t_present, minibatch_size, verbose):

        l_step = []
        # words
        w_pred = []
        w_true = []

        # vectors
        v_pred = []
        v_true = []

        # losses = []
        p_recs = []
        successes = []

        print(f"Running {learner} ...")
        # for n_step in tqdm(range(1, time)):
        for n_step in tqdm(range(time)):

            # Save history.
            # history.weights[n_step] = history.weights[n_step - 1].copy()  # XXX
            # history.neurons[n_step] = history.neurons[n_step - 1].copy()  # XXX
            # history.weights = torch.stack((history.weights, other_tensor))

            # Forget: update model weights toward noise. Always happens.
            # forgetting.step(n_step)

            sleeping = 0
            if not learner.learning(n_step):
                sleeping += 1
            if sleeping > 100:
                breakpoint()

            # Learn if session time.
            if learner.learning(n_step):
                # for _ in range(minibatch_size):
                # XXX SEE IF LEARNING.STEP() IS TAKING CORRECT PATTERN
                l_step.append(n_step)

                # GUI: Kanji and 6 possible replies displayed.
                item, pos_replies = teacher(n_step)
                if not is_synth:
                    pattern = encode(item, size=n_features)
                else:
                    pattern = item
                attr = Attractors.encode(pattern)

                # GUI: User sees the kanji, decides best answer, and replies.
                # reply = learner.reply(item, pos_replies=pos_replies)
                rep_args = dict(question=item, pos_replies=pos_replies)
                if param_rec:
                    rep_prec = data_prec.at[n_step, "w_reply"]
                    rep_args = {**rep_args, **dict(reply_prec=rep_prec)}

                # reply, p_recall = learner.reply(item, pos_replies=pos_replies)
                reply, p_recall = learner.reply(**rep_args)
                reply_true = teacher.reply(n_step)
                if is_synth:
                    success = p_recall > 0.8
                else:
                    success = reply == reply_true
                # log_l = log_likelihood(p_recall)

                # History.
                v_pred.append(learner.last_predicted)
                v_true.append(pattern)
                p_recs.append(p_recall)
                w_pred.append(reply)
                w_true.append(reply_true)
                successes.append(success)

                # Learn: update weights toward those of the presented item.
                learning.step(pattern)

                # Metrics.
                # Success or failure determine if loss is reward.

                if verbose:
                    # print(f"Attractor:\n{attrac[self.n_item]}")
                    print(f"Answer:\t{reply}")
                    print(f"I WAS learning item {item}!")

            # if verbose:
            # print(f"Weight:\n{tuple(learner.bag_neur.parameters())}")
        log_like = log_likelihood(p_recs)
        rate_learning = session.params.rate_learning
        rate_forgetting = session.params.rate_forgetting
        results = dict(
            w_true=w_true,
            w_reply=w_pred,
            p_rec=p_recs,
            success=successes,
            log_likelihood=log_like,
            rate_learning=rate_learning,
            rate_forgetting=rate_forgetting,
        )
        index = pd.Index(l_step, name="l_step")
        results = pd.DataFrame(results, index=index)
        print("Done!")
        return results if trial is None else log_like

    results = simulate(data_prec=data_prec)

    return results


def make_synth(n_patterns, rep_size, from_subject, n_replies, pat_gen):

    # Length and break periods.
    if from_subject:
        data_user = loader.prepare_user_data()
        t_steps = data_user.index
        n_steps = len(t_steps)

    def reply_from_pattern(pattern, n_replies, rng):
        pat = pattern[0].copy()
        pats = []
        for _ in range(n_replies):
            rng.shuffle(pat)
            pats.append(pat.copy())

        # Reply order randomized for indexing.
        n_pos_reps = np.arange(1, n_replies + 1)
        rng.shuffle(n_pos_reps)

        # Naming replies.
        pref = "pos_reply"
        n_pos_reps = ["_".join((pref, str(npr))) for npr in n_pos_reps]
        pos_rep_pat = dict(zip(n_pos_reps, pats))

        # Correct answer.
        # corr = next(iter(pos_rep_pat.values()))
        corr = pattern[0].copy()
        q_entry = dict(item=corr, **pos_rep_pat)
        return q_entry

    # Correct answers.
    items = [
        pat_gen.random_binary(size=rep_size, n_patterns=1) for n in range(n_patterns)
    ]
    count = 0
    entries = []
    for item in cycle(items):  # repeat pattern as needed.
        entry = reply_from_pattern(pattern=item.copy(), n_replies=n_replies, rng=rng)
        entries.append(entry)
        count += 1
        if count == n_steps:
            break

    entries = pd.DataFrame(entries)
    entries.index = t_steps

    return entries


# users = loader.get_data()["user"].unique()

# Now generate the data
rep_size = 10
n_replies = 6
pat_gen = PatternsGenerator(random_state=0)
rng = np.random.default_rng(0)
n_patterns = 10

data = make_synth(
    n_patterns=n_patterns,
    rep_size=rep_size,
    from_subject=True,
    n_replies=n_replies,
    pat_gen=pat_gen,
)
# data = data.iloc[: len(data) // 2]  # THIS TO CUT TASK


# Then agent parameters
from dataclasses import dataclass


@dataclass
class Param:

    min: float
    max: float
    name: str

    @property
    def bounds(self):
        return self.min, self.max


@dataclass
class HistorySim:
    param: Param
    sim: pd.DataFrame = None
    rec: pd.DataFrame = None
    name: str = None

    # param: Param  # = pd.DataFrame(index=index, columns=cols)
    # _index = pd.Index(range(n_ags), name="n_agent", dtype=int)
    # values = pd.Series(index=_index, name=param.name)


# Param ranges.
l_rs = Param(0.0, 1.0, name="rate_learning")
f_rs = Param(0.5, 0.5, name="rate_forgetting")
params = l_rs, f_rs
# params = [HistorySim(p) for p in params]

# param = ParamSim(param=l_rs)  # , sim=param_sim, rec=param_rec)

# Agents to simulate
n_ags = 20
n_rec_trials = 1000

all_sim = []
all_optim = []
rng = np.random.default_rng(0)

index = pd.Index(range(n_ags), name="n_agent", dtype=int)
cols = pd.Index([p.name for p in params], name="param", dtype=str)
param_sim = pd.DataFrame(index=index, columns=cols)
param_rec = pd.DataFrame(index=index, columns=cols)

param_sims = pd.DataFrame()
param_recs = pd.DataFrame()


for n_ag in tqdm(range(n_ags)):
    params_to_sim = {p.name: rng.uniform(*p.bounds) for p in params}

    # Simulate to get data first.
    results_sim = sim(params=params_to_sim, data_user=data)
    all_sim.append(results_sim)

    # Recover with optim.
    study = optuna.create_study(direction="maximize")
    study.optimize(
        lambda trial: sim(trial, data_prec=results_sim, data_user=data),
        n_trials=n_rec_trials,
    )
    all_optim.append(study)

    # Save.
    param_sim.loc[n_ag] = pd.Series(params_to_sim)
    param_rec.loc[n_ag] = pd.Series(study.best_params)
    # param_sims = pd.concat((param_sims, param_sim))
    # param_recs = pd.concat((param_recs, param_rec))


param_sim.to_csv(_paths.RESULTS_DIR / f"{TODAY}-param-sims.csv")
param_rec.to_csv(_paths.RESULTS_DIR / f"{TODAY}-param-recs.csv")

#%%

recs = pd.melt(param_rec)
# recs["type"] = "recovered"

recs

#%%
sims = pd.melt(param_sim)
# sims["type"] = "simulated"

#%%
import pandas as pd
import seaborn as sns
from lib.utils import _paths
# main = lambda: None

sims = pd.read_csv(_paths.RESULTS_DIR / f"{TODAY}-param-sims.csv", index_col=0)
recs = pd.read_csv(_paths.RESULTS_DIR / f"{TODAY}-param-recs.csv", index_col=0)

recs = pd.melt(recs)
# # recs["type"] = "recovered"
#
sims = pd.melt(sims)
# # sims["type"] = "simulated"

print(recs)
print(sims)

results = pd.merge(
    sims,
    recs["value"],
    left_index=True,
    right_index=True,
    suffixes=["_simulated", "_recovered"],
)
# results["param"] = results["value"]
# print(results)
# results = pd.melt(results, id_vars=["variable"], value_vars=["value_simulated", "value_recovered"], var_name="param")
# print(results)

#%%
# results = pd.concat((sims, recs))

#%%


f_g = sns.relplot(
    x="value_simulated", y="value_recovered", col="variable", data=results, kind="scatter"
)
# f_g.refline()#x=0.0, y=0.0)# (x=np.array([0.0, 1.0]), y=np.array([0.0, 1.0]))
f_g.map(sns.lineplot, x=[0, 1], y=[0, 1])  # , ':y',) transform=f_g.ax_joint.transAxes))
f_g.savefig(_paths.FIGURES_DIR / f"{TODAY}-param-recovery.png")

#%%
# import os
#
# path = f"./{TODAY}/"
# os.makedirs(path, exist_ok=True)
#
# results_sim.to_csv(path + f"{n_ag}-results-sim.csv")
#
#
# best_params = study.best_params
# with open(
#     _paths.OPTIMIZATION_DIR / f"{n_ag}-best-params.json", "w", encoding="utf-8"
# ) as file:
#     json.dump(best_params, file)
# #%%
# path
# #%%
#
# # from itertools import product
#
# # results_ll = #simulate(data_prec=results)
# # print(l_rs)
#
#
# #%%
#
# # NOW: record the p of giving the same answer as the results to the simulation.
# #%%
#
# log_likelihood(p_recs)
#
# #%%
#
# format_kws = dict(kind="line")
# f_grid = sns.relplot(x=results.index, y="p_rec", data=results, **format_kws)
# path = "./figures/" + "p_rec.jpg"
# f_grid.savefig(path)
#
#
# #%%
#
# import optuna
#
#
# def model_comps(n_features):
#     data_user = loader.prepare_user_data()
#
#     parameters = Parameters()
#
#     # Patterns.
#
#     # patterns = PatternsGenerator(size=n_features, n_patterns=3, random_state=1).random_binary()
#     patterns = PatternsGenerator(
#         size=n_features, n_patterns=3, random_state=1
#     ).user_fakes(data_user["item"])
#
#     # n_features = patterns[0].size()
#     if isinstance(n_features, torch.Size):
#         (n_features,) = list(n_features)
#
#     # model = AttractorNet(n_features=n_features)
#     # learner = Learner(model, data=data_user, verbose=True)
#     learner = AttractorNet(n_features=n_features, agent_data=data_user, verbose=True)
#     for param in learner.parameters():
#         param.requires_grad = False
#
#     # Time
#     time = parameters.t_discrete  # This is a session parameter, not a model param.
#     t_present = int(parameters.t_presentation)  # WON'T WORK dt != 1.0 XXX
#
#     # Learning items.
#     n_items = len(patterns)  # XXX
#     n_item = 0  # First item to present.
#     epoch = 0  # For history.
#     minibatch_size = 1  # Items per learning session.
#
#     n_items = 3  # XXX to avoid 1412 items for now
#
#     # Hyperparameters.
#     session = Hyperparameters(
#         n_features,
#         parameters,
#         patterns,
#         random_state=123,
#         verbose=False,
#         data=data_user,
#     )
#     # history = History(time, n_features)
#     time = session.time
#
#     # Optimizers.
#     param_update = [
#         {"params": tensor, "name": name}
#         for name, tensor in dict(learner.named_parameters()).items()
#     ]
#     learning = Learning(param_update, session.params, session.attractors)
#     forgetting = Forgetting(param_update, session.params, session.noise)
#
#     # teacher = Teacher([0, 1, 2], minibatch_size)
#     teacher = Teacher(data=data_user, verbose=True)
#     verbose = True
#
#     loss_fn = nn.MSELoss()
#     return learner, learning, forgetting
#
#
# def objective(trial, model, n_features):
#
#     x_s = np.zeros(n_features)
#
#     ints = -1, 1
#     for idx in range(n_features):
#
#         x_s[idx] = trial.suggest_int(f"x_{idx}", *ints)
#
#     # careful taking model from global
#     reply, _ = model.reply(item)
#
#     loss = mse(reply, x_s)
#
#     return loss
#
#
# def main():
#     # learner, learning, forgetting = model_comps(n_features=5)
#     obj = partial(objective, model=learner, n_features=n_features)
#     study = optuna.create_study(direction="minimize")
#     study.optimize(obj, n_trials=100)
#     print(study.best_trial)
#     return study
#
#
# study = main()
# #%%
#
# # dir(study)
# study.best_trial.values
#
# #%%
# # reply_info = {**learner.identity, **learner.conditions, **teacher.answers[n_step], reply, teacher.evaluation(reply)}
# # reply_info = {**learner.identify(), **learner.get_conditions()}
# #%%
# ########### LEARNING PARAMSSSSSSS
# from lib.ops import _int_to_bin_arr
#
# """
# w_pred : list of str
#     Predicted word.
# w_true : list of str
#     True word.
# v_pred : list of str
#     Predicted (word)-vector.
# v_true : list of str
#     True (word)-vector.
# """
#
# kanji_df = teacher.question_data.copy()
# user_df = teacher.data.copy()
# #%%
# n_features = 200
# s2p = lambda x: cgn.item_to_bin_arr(x, n_features)
# s2i = cgn.word_to_int
# res = kanji_df["meaning"].apply(s2i)
#%%


#%%
#%%


#%%
# # Infinite pattern generator
# t_steps
# rep_size = 5
# n_replies = 4
# pat_gen = PatternsGenerator(random_state=0)
# rng = np.random.default_rng(0)


# from_subject = True
# if from_subject:
#     data_user = loader.prepare_user_data()
#     t_steps = data_user.index
#     n_steps = len(t_steps)


# def get_entry(*, rng, pat_gen, n_replies, rep_size):

#     if isinstance(pat_gen, np.ndarray):
#         pats = pat_gen
#     elif isinstance(pat_gen, object):
#         pats = pat_gen.random_binary(size=rep_size, n_patterns=n_replies)
#     else:
#         raise TypeError("pat_gen must be a pattern generator or pattern")
#     n_pos_reps = np.arange(1, n_replies + 1)
#     rng.shuffle(n_pos_reps)

#     pref = "pos_reply"
#     n_pos_reps = ["_".join((pref, str(npr))) for npr in n_pos_reps]
#     pos_rep_pat = dict(zip(n_pos_reps, pats))
#     # }{npr: rep for npr, rep in zip(n_pos_reps, pats)}
#     corr = next(iter(pos_rep_pat.values()))
#     q_entry = dict(item=corr, **pos_rep_pat)
#     return q_entry
# entries = []
# for n_step in range(n_steps):
#     entry = get_entry(rng=rng, pat_gen=pat_gen, n_replies=n_replies, rep_size=rep_size)
#     entries.append(entry)

# synth = pd.DataFrame(entries)
# synth.index = t_steps

# synth
