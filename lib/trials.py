"""
    attractor-network-memory.trials
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    DESCRIPTION

    :copyright: (c) 2021 by YOUR_NAME.
    :license: LICENSE_NAME, see LICENSE for more details.
"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import torch
from scipy.optimize import minimize
from scipy.special import beta as beta_fn
from scipy.stats import beta, norm, weibull_min
from torch import nn
from torch.distributions import Beta, Weibull
from torch.functional import F
from tqdm import tqdm

plt.close("all")
sns.set_theme()
sns.set_context("paper")

# rng = np.random.default_rng(0)

# vector = rng.normal(size=10)
##%%


##%%


##%%


# n = 100  # number of samples
# k = 2.4  # shape
# lam = 5.5  # scale

# x = weibull_min.rvs(k, loc=0, scale=lam, size=n)
##%%

# axis = np.arange(len(x))
##%%
# fig = sns.relplot(x=axis, y=x, kind="line")
# fig.savefig("plot.jpg")


##%%

# weibull_min.pdf

##%%

# speeds = np.linspace(0, 2.5, 1000)
# p = weibull_min.pdf(speeds, c=1, scale=1)

# fig = sns.relplot(x=speeds, y=p, kind="line")
# fig.savefig("plot.jpg")

##%%

# plt.close("all")
# X = np.linspace(0, 0.9, 1000)
# y_true = norm.pdf(speeds, loc=1, scale=0.3)

# c = 1
# scale = 1
# scale, c = 1.7816e-03, 5.3845e-06
# y_pred = weibull_min.pdf(speeds, c=c, scale=scale)


# fig = sns.relplot(x=X, y=y_pred, kind="line")
# fig.savefig("plot.jpg")

##%%

# n = 1000
# noise = torch.Tensor(np.random.normal(0, 0.02, size=n))
# x = torch.arange(n) + 1
# a, k, b = 0.7, 0.01, 0.2
# y = a * np.exp(-k * x) + b + noise
# plt.figure(figsize=(14, 7))
# plt.scatter(x, y, alpha=0.4)

#%%


def weibull_pdf(vals, scale, shape):
    r"""Manual Weibull PDF so it records tensor operations.

    lambda \in (0 \pm + \inf) : scale
    k \in (0, + \inf) : shape

    Parameters
    ----------
    vals : torch.Tensor
    scale : float
    shape : float

    Returns
    -------
    torch.Tensor

    """

    scaled = vals / scale

    term_0 = (shape / scale) * (scaled) ** (shape - 1)
    term_1 = np.e ** -((scaled) ** scale)

    vals = term_0 * term_1
    return vals


class Model(nn.Module):
    """Custom Pytorch model for gradient optimization."""

    def __init__(self, distribution, init_guess):
        """
        Parameters
        ----------
        distribution : torch.

        """

        super().__init__()
        self.weights = nn.Parameter(init_guess)
        self.distrib = distribution

    def forward(self, vals):
        """Implement function to be optimised. In this case, an exponential decay
        function (a + exp(-k * X) + b),
        """

        param_0, param_1 = self.weights

        # Remove < 0.
        max_zero = torch.heaviside(vals, torch.Tensor((1,)))
        vals *= max_zero

        # Log_prob gives the log of the PDF.
        vals = np.e ** self.distrib(param_0, param_1).log_prob(vals)
        return vals


def training_loop(model, x_train, y_train, optimizer, loss_fn, n=1000, breaking=False):
    "Training loop for torch model."
    losses = []
    for i in tqdm(range(int(n))):
        preds = model(x_train)
        loss = loss_fn(preds, y_train)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
        losses.append(loss)
        if breaking and n > 1:
            break
        if not i % 1000:
            print(loss)
            print(model.weights)
    return model, losses


# Data.
x_train = np.linspace(0.01, 2.5, 1000)
x_train = torch.Tensor(x_train)
x_train = nn.functional.normalize(x_train, dim=0)
# y_true = norm.pdf(x_train, loc=0.75, scale=0.2)
# y_true = norm.pdf(x_train, loc=0.5, scale=0.4)
# y_true = norm.pdf(x_train, loc=0.05, scale=0.002)
y_true = weibull_min.pdf(x_train, c=0.5, scale=1.0)

y_train = torch.Tensor(y_true)

#%%
# Model.
init_guess = torch.Tensor([1.5, 2.5])
model = Model(distribution=Beta, init_guess=init_guess)
opt = torch.optim.Adam(model.parameters())  # , lr=0.1)# , weight_decay=0.01)
loss_fn = nn.MSELoss()
model, losses = training_loop(
    model, x_train, y_train, opt, loss_fn=loss_fn, n=10000  # , breaking=True
)

#%%

# Plot.
plt.close("all")
# X = np.linspace(0, 2.5, 1000)
to_np = lambda x: x.detach().numpy()
# model.weights = torch.nn.Parameter(torch.Tensor([1.0, 1.5]))
y_pred = to_np(model(x_train))
# y_pred = to_np(np.e ** Weibull(1,1.5).log_prob(x_train))
x_train_ar = to_np(x_train)
# y_true = norm.pdf(x_train, loc=0.5, scale=0.4)

# fig = sns.relplot(x=x_train_ar, y=y_true, kind="line")
fig = sns.relplot(x=x_train_ar, y=y_pred, kind="line")
fig.savefig("plot.jpg")


#%%
# Scipy fit.

shape, loc, scale = weibull_min.fit(y_train)
print(shape, loc, scale)
y_pred = weibull_min.pdf(x_train, shape, loc, scale)

#%%
# Plot.
plt.close("all")
x_train_ar = to_np(x_train)

# fig = sns.relplot(x=x_train_ar, y=y_true, kind="line")
fig = sns.relplot(x=x_train_ar, y=y_pred, kind="line")
fig.savefig("plot.jpg")

#%%
# Scipy Beta fit.

alp, bet, loc, scale = beta.fit(y_train)
print(alp, bet, loc, scale)
y_pred = beta.pdf(x_train, alp, bet, loc, scale)

#%%
# Plot.
plt.close("all")
x_train_ar = to_np(x_train)

# fig = sns.relplot(x=x_train_ar, y=y_true, kind="line")
fig = sns.relplot(x=x_train_ar, y=y_pred, kind="line")
fig.savefig("plot.jpg")

#%%
# Beta trials. MEH


def beta_pdf(vals, conc_0, conc_1):
    """Homemade version of the beta distribution pdf.

    Parameters
    ----------
    vals : TODO
    conc_0 : TODO
    conc_1 : TODO

    Returns
    -------
    TODO

    """
    pass
    bet_val = beta_fn(conc_0, conc_1)
    first = (1 / bet_val) * vals ** (conc_0 - 1)
    second = (1 - vals) ** (conc_1 - 1)
    vals = first * second
    return vals
