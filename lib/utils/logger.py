"""
    attractor-network-memory.logger
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Logging utils.

    :copyright: (c) 2021 by Carlos de la Torre-Ortiz.
    :license: GPLv3, see LICENSE for more details.
"""


import datetime
import logging
import os

import numpy as np
import pandas as pd


def logger(func: callable) -> callable:
    """Decorator to log function calls."""

    logging.basicConfig(
        filename=os.path.join("./logs", f"{func.__name__}.log"), level=logging.INFO
    )

    def wrapper(*args, **kwargs):
        now = datetime.datetime.now().isoformat()
        logging.info(f"%s: Run with args {args} and kwargs{kwargs}", now)
        return func(*args, **kwargs)

    return wrapper


def get_mat_shape(n_steps, n_features):
    """Get the shape of the tensor holding matrices over time."""

    shape_t = (n_steps,)  # (int,)
    shape_attrac = (n_features,)  # (int,)
    return shape_t + shape_attrac + shape_attrac


class History:
    """Keeps track of the model parameters over iterations."""

    def __init__(self, n_steps, n_features):
        self._n_steps = n_steps
        self._n_features = n_features

        self.weights = None  # (n_features, n_features, t_total)
        self.weights_res = None  # (n_features, n_features, t_total)
        self.neurons = None  # (n_features,)

        # Calls
        self._init_weights()
        self._init_neurons()

        # Plotting
        self.loss_df = pd.DataFrame()

    def _init_weights(self):
        """Set the initial values for weights at 0."""

        shape = get_mat_shape(self._n_steps, self._n_features)  # (int, int, int)
        self.weights = np.zeros(shape)
        self.weights_res = np.zeros(shape)  # connection resistance

    def _init_neurons(self):
        """Set the initial values for neurons at 1."""

        shape = get_mat_shape(self._n_steps, self._n_features)[:-1]  # (int, int)
        self.neurons = np.ones(shape)
