"""
Copyright (C) <2022>  <de la Torre-Ortiz C>
GPLv3+. See the 'LICENSE' file for details.

Paths information.
"""

from pathlib import Path


def make_and_return(path):
    """Make the directory but also return the path.

    Parameters
    ----------
    path : pathlib.Path

    Returns
    -------
    pathlib.Path

    """

    path.mkdir(exist_ok=True)
    return path


mkdir = make_and_return

# BASE_DIR = Path(__file__).resolve().parent.parent.parent
BASE_DIR = Path(__file__).resolve().parents[2]
# CONFIG_FILE = BASE_DIR / "config.yml"
FIGURES_DIR = mkdir(BASE_DIR / "figures")
# LOGS_DIR = mkdir(BASE_DIR / "logs")
# DATASETS_DIR = mkdir(BASE_DIR / "datasets")
# OPTIMIZATION_DIR = mkdir(BASE_DIR / "optimization")
# MODELS_DIR = mkdir(BASE_DIR / "models")
# STATS_DIR = mkdir(BASE_DIR / "stats")

DATA_DIR = mkdir(BASE_DIR / "data")
RESULTS_DIR = mkdir(BASE_DIR / "results")
