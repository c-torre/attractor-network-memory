"""
    lib.core
    ~~~~~~~~~~~~~~

    Core utils.

    :copyright: (c) 2022 by Carlos de la Torre-Ortiz.
    :license: GPLv3, see LICENSE for more details.
"""

import datetime

TODAY = datetime.date.today().isoformat().replace("-", "")
