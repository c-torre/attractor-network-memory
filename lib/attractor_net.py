import numpy as np
import torch
import torch.nn.functional as F
from torch import nn
from torch.nn.parameter import Parameter

from lib.agents import Learner
from lib.ops import CognitiveNet


def heavinorm(preactivations):
    """
    Currents to activations.
    (n_features) = (n_features, n_features) @ (n_features,)
    """

    values = torch.ones_like(preactivations)
    return torch.heaviside(
        preactivations / np.linalg.norm(preactivations), values
    ).int()


class Bag(nn.Module):
    """Fully connected bag of neurons that are
    simultaneously the input and output layer.
    """

    __constants__ = ["n_features"]

    def __init__(
        self,
        n_features: int,
        bias: bool = False,
        device=None,
        dtype=None,
    ) -> None:

        factory_kwargs = {"device": device, "dtype": dtype}
        # super(Bag, self).__init__()
        super().__init__()
        self.n_features = n_features
        self.weight = Parameter(torch.zeros((n_features, n_features), **factory_kwargs))
        if bias:
            self.bias = Parameter(torch.empty(n_features, **factory_kwargs))
        else:
            self.register_parameter("bias", None)
        self.reset_parameters()

    def reset_parameters(self):
        """Reset parameters."""

        if self.bias is not None:
            self.weight *= 0

    def forward(self, values):
        """Compute forward pass."""

        forw = F.linear(values, self.weight, self.bias)
        return forw

    def extra_repr(self):
        return "n_features={}, bias={}".format(self.n_features, self.bias is not None)


class AttractorNet(Learner, nn.Module, CognitiveNet):
    """Main attractor net class."""

    def __init__(
        self, n_features, *, agent_id=None, agent_data=None, verbose=False
    ):  # init_guess
        """
        Parameters
        ----------
        n_features : int
        agent_id : str, optinal
        agent_data : pandas.DataFrame, optional
        verbose : bool, optional
        """

        if not isinstance(n_features, int):
            raise ValueError("n_features must be int")
        if agent_data is not None and len(agent_data) == 0:
            raise ValueError("Agent data seems empy")

        Learner.__init__(self, id_=agent_id, data=agent_data, verbose=verbose)
        nn.Module.__init__(self)
        CognitiveNet.__init__(self, n_features)
        # super(AttractorNet, self).__init__()
        self.bag_neur = Bag(n_features, dtype=float)
        self.bag_res = Bag(n_features, dtype=float)
        # self.similarities = nn.Parameter(init_guess)
        self.last_predicted = None

    def forward(self, pattern):
        pred = self.bag_neur(pattern)
        pred = heavinorm(pred)
        self.last_predicted = pred
        return pred
