"""
    attractor-network-memory.utils
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Utils related to user data.

    :copyright: (c) 2021 by Carlos de la Torre-Ortiz.
    :license: GPLv3, see LICENSE for more details.
"""

import numpy as np
import pandas as pd


def average_answer_time(data_users):
    """Get the average time all users took to answer.
    Userful for simulation.

    Parameters
    ----------
    data_users : pandas.DataFrame

    Returns
    -------
    float

    """

    diffs = []
    for _, session_data in data_users.groupby("session"):
        diffs.append(np.diff(session_data["timestamp"]).mean())
    diffs = np.mean(diffs)
    return diffs


def change_time_base(timestamps, min_elapsed_time=7):
    """Change the base of current time stamps to normalized by some time delta.
    It makes sure that resulting outputs are all unique. Necessary to account
    for breaks e.g:

    timestamps = pd.Series([0.3, 6.7, 3.8, 70.2, 77.1])
    min_elapsed_time: 7
    return: pd.Series([0, 1, 2, 10, 11])

    Parameters
    ----------
    timestamps : pandas.Series
    min_elapsed_time : int, optional
        Default value is the average time all users took to answer

    Returns
    -------
    pandas.Series

    """

    if not isinstance(min_elapsed_time, int):
        raise TypeError("Minimum elapsed time must be int")
    if not timestamps.is_monotonic_increasing:
        raise ValueError("Series must be monotonically increasing")

    normalized = timestamps - timestamps.iloc[0]

    normalized = np.round(normalized / min_elapsed_time).astype(int)
    normalized *= min_elapsed_time
    _, indices = np.unique(normalized, return_index=True)

    where_unique = np.zeros(len(normalized), dtype=int)
    where_unique[indices] = 1
    where_duplicate = 1 - where_unique

    to_add = where_duplicate * min_elapsed_time
    to_add = np.cumsum(to_add)
    rebased = normalized + to_add
    rebased /= min_elapsed_time
    rebased = rebased.rename("t_discrete").astype(int)

    if not len(timestamps) == len(np.unique(rebased)):
        raise ValueError("Rebased series is not the same length as original")

    return rebased


def reformat_data(data, min_elapsed_time=7):
    """Modify the data to make the simulation possible

    1. Set a new column with consistent discretized time that
    also accounts for rest periods.
    2. Make the username and these time steps as multiindex.

    Parameters
    ----------
    data : pandas.DataFrame

    Returns
    -------
    pandas.DataFrame
    """

    data = data.sort_index()
    data_discrete = []
    for _, data_user in data.groupby("user"):
        data_user["t_step"] = change_time_base(data_user["timestamp"], min_elapsed_time)
        data_discrete.append(data_user)
    data = pd.concat(data_discrete)
    data = data.set_index(["user", "t_step"])
    return data
