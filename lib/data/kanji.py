"""
    attractor-network-memory.kanji
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Load the kanji database.

    :copyright: (c) 2021 by Carlos de la Torre-Ortiz.
    :license: GPLv3, see LICENSE for more details.
"""

import json

import pandas as pd


def _create_wk_entries(path="data/wanikani.json"):
    """Get kanji data from JSON raw.

    Parameters
    ----------
    path : str, optional
        Path to raw JSON with kanji data.

    Returns
    -------
    pandas.DataFrame

    """

    with open(path, "r", encoding="utf-8") as kan:
        data = json.load(kan)

    entries = []
    for dat in data["requested_information"]:
        dat.pop("user_specific", None)
        for k in ("meaning", "onyomi", "kunyomi", "nanori"):
            if dat[k] is None:
                dat.pop(k)
            else:
                dat[k] = dat[k].split(", ")
        entries.append(dat)
    data = pd.DataFrame.from_dict(entries)

    return data


def _prepare_data(data):
    """Select only relevant columns from kanji data.
    Name the index.

    Parameters
    ----------
    data : pandas.DataFrame

    Returns
    -------
    pandas.DataFrame

    """

    # Only some columns are interesting now.
    selection = "level", "character", "meaning"
    data = data.loc[:, selection]

    # ID explicit for clarity and convenience.
    data.index = data.index.rename("id")

    # Only first meaning taken.
    data["meaning"] = [m[0] for _, m in data["meaning"].iteritems()]

    return data


def get_data():
    """Load and preprocess data.

    Returns
    -------
    TODO

    """

    data_list = _create_wk_entries()
    data_df = _prepare_data(data_list)

    return data_df


if __name__ == "__main__":
    kanji_df = get_data()
