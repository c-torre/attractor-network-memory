FROM python:slim-buster
WORKDIR /app
RUN apt-get update -y
COPY . .
RUN pip3 install --user -r requirements.txt
