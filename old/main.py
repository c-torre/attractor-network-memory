#!/bin/python
"""
Copyright (C) <2021>  <de la Torre-Ortiz C>
GPLv3+. See the 'LICENSE' file for details.

Attractor network that learns and forgets.
"""

from functools import cache

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from lib.metrics.mean_squared_error import (mean_squared_error,
                                            mean_squared_error_min_inv)
from lib.model.attractors import Attractors
from lib.model.parameters import Parameters
from lib.model.patterns import PatternsGenerator
from lib.plot.slider import imshow_slider
from lib.utils.logger import logger
from numpy.random import default_rng
# from sklearn.preprocessing import normalize
from scipy.special import expit
from scipy.stats import truncnorm
from tqdm import tqdm

plt.close("all")


def normalize(array, min_=0.0, max_=1.0):
    """Minmax normalize numpy.array.

    Parameters
    ----------
    array : numpy.ndarray
    min_ : float, default=0
    max_ : float, default=1

    Returns
    -------
    numpy.ndarray

    """

    eps = np.finfo(float).eps
    array = array.astype(float)

    array -= array.min()
    array *= max_ - min_
    array /= array.ptp() + eps

    return array


def adam(m, v, res, t_step, beta1, beta2, eps=1e-8):
    return adaval


class AttractorNet:
    """Main attractor net class."""

    def __init__(
        self,
        parameters: Parameters,
        patterns: np.ndarray,
        seed: int = 123,
        verbose: bool = False,
    ):

        # Init
        self.seed = seed
        self._rng: default_rng = default_rng(seed)
        self._verbose: bool = verbose

        self.parameters: Parameters = parameters
        self.patterns: np.ndarray = patterns  # (num_patterns, pattern_size)
        self.attractors: Attractors = Attractors(self.patterns)

        # State
        self.noise: np.ndarray = None  # (num_neurons, num_neurons, t_total)
        self.weights: np.ndarray = None  # (num_neurons, num_neurons, t_total)
        self.weights_str: np.ndarray = None  # (num_neurons, num_neurons, t_total)
        self.neurons: np.ndarray = None  # (pattern_size,)

        # Plotting
        self.loss_df: pd.DataFrame = pd.DataFrame()

        # Calls
        self._init_noise()
        self._init_weights()
        self._init_neurons()

    def __str__(self):
        return f"AttractorNet with {self.parameters}"

    def __repr__(self):
        return self.__str__()

    def __call__(self):
        self.run()

    def _init_noise(self) -> None:
        """Compute the noise for all iterations."""

        rng = self._rng

        shape_attrac = self.attractors.patterns[0].shape  # (int, int)
        shape_t = (self.parameters.t_discrete,)  # (int,)
        shape = shape_t + shape_attrac

        # self.noise = rng.normal(size=shape)
        # self.noise = truncnorm.rvs(-1, 1, size=shape, random_state=self.seed)
        self.noise = rng.uniform(-1, 1, size=shape)

    def _init_weights(self) -> None:
        """Set the initial values for weights: first iter 0, rest None."""

        shape_attrac = self.attractors.patterns[0].shape  # (int, int)
        shape_t = (self.parameters.t_discrete,)  # (int,)
        shape = shape_t + shape_attrac

        self.weights = np.zeros(shape)
        self.weights_str = np.zeros(shape)

    def _init_neurons(self) -> None:
        """Set the initial values for neurons."""

        shape_attrac = (self.attractors.patterns[0].shape[0],)  # (int)
        shape_t = (self.parameters.t_discrete,)  # (int,)
        shape = shape_t + shape_attrac

        self.neurons = np.ones(shape)

    @property
    def patterns(self) -> np.ndarray:
        """Get patterns."""

        return self._patterns

    @patterns.setter
    def patterns(self, patterns: np.ndarray) -> None:
        """Set patterns if not empty."""

        if patterns.size:
            self._patterns = patterns
        else:
            raise ValueError("Patterns must not be empty")

    @staticmethod
    def get_activations(preactivations: np.ndarray) -> np.ndarray:
        """
        Currents to activations.
        (num_neurons) = (num_neurons, num_neurons) @ (num_neurons,)
        """

        return np.heaviside(preactivations / np.linalg.norm(preactivations), 1).astype(
            int
        )  # EXPERIMENTAL

        # return np.heaviside(preactivations, 1).astype(int) # REGULAR VERSION

    def query(self, pattern, verbose=False):

        wgt = self.weights[-1]
        # f_mse = mean_squared_error_min_inv
        f_mse = mean_squared_error

        pre_act = wgt @ pattern
        neur_curr = self.get_activations(pre_act)
        if verbose:
            loss = f_mse(pattern, neur_curr)
            print(f"Preactivations:\n{pre_act}")
            print(f"Loss (MSE):\n{loss}")
        return neur_curr

    @logger
    def run(self) -> None:
        """
        Run simulation, at each iteration:

        - Update weights -> learning or forgetting
        - Update neurons -> pattern retrieval for performance
        """

        verbose = self._verbose

        # State
        neur: np.ndarray = self.neurons
        patts: np.ndarray = self.patterns
        attrac: np.ndarray = self.attractors.patterns
        wgt: np.ndarray = self.weights
        noi: np.ndarray = self.noise

        # Func
        f_act: callable = self.get_activations
        f_mse: callable = mean_squared_error_min_inv

        # Time
        time: int = self.parameters.t_discrete
        t_present = int(self.parameters.t_presentation)  # WON'T WORK dt != 1.0 XXX
        n_items = len(self.patterns)  # XXX
        n_item: int = 0  # debug item to learn

        # Learning and forgetting
        l_r: float = self.parameters.rate_learning
        f_r: float = self.parameters.rate_forgetting

        # FOR A DEFINED MEM MIN MAX
        att_sum = self.attractors.cumsum[-1] * 2  # was 6
        mem_min_max = att_sum.min(), att_sum.max()

        # Adam
        m = np.zeros_like(wgt)
        v = np.zeros_like(wgt)
        beta1 = 0.8
        beta2 = 0.999

        # Session

        print(f"Running {self} ...")
        for t_step in tqdm(range(1, time)):

            # Avoid constant slicing and set current as previous.
            wgt_prev = wgt[t_step - 1]
            wgt[t_step] = wgt_prev.copy()
            wgt_curr = wgt[t_step]

            neur_prev = neur[t_step - 1]
            neur[t_step] = neur_prev.copy()
            neur_curr = neur[t_step]

            # Update weights (memory)...
            # ...with noise (forgetting).
            # wgt_curr += noi[t_step].copy() * f_r  BEFORE trial
            # wgt_curr += noi[t_step].copy() * f_r * (1 - self.weights_str[t_step])
            # normalize(noi[t_step].copy())
            # noi_adap = noi[t_step].copy() * f_r * (1 - self.weights_str[t_step])
            # res = expit(self.weights_str[t_step]) * 10 - 5
            # res = np.e ** (-self.weights_str[t_step] ** 2) # gaussian activation
            self.weights_str[t_step] = self.weights_str[t_step - 1].copy()
            res = self.weights_str[t_step].copy()

            ### Adam
            # m(t) = beta1 * m(t-1) + (1 - beta1) * g(t)
            m[t_step] = beta1 * m[t_step] + (1.0 - beta1) * wgt_curr
            # v(t) = beta2 * v(t-1) + (1 - beta2) * g(t)^2
            v[t_step] = beta2 * v[t_step] + (1.0 - beta2) * wgt_curr ** 2
            # mhat(t) = m(t) / (1 - beta1(t))
            mhat = m[t_step] / (1.0 - beta1 ** (t_step + 1))
            # vhat(t) = v(t) / (1 - beta2(t))
            vhat = v[t_step] / (1.0 - beta2 ** (t_step + 1))
            # x(t) = x(t-1) - alpha * mhat(t) / (sqrt(vhat(t)) + eps)
            adaval = mhat / (np.sqrt(vhat) + 1e-8)
            # my addition
            ### End Adam
            # adaval = np.e ** (- adaval ** 2)
            res = np.e ** (-0.1 * res ** 2)

            noi_adap = noi[t_step].copy() * f_r * res
            # noi_adap = expit(noi_adap) * 10 - 5

            # if t_step == 50:
            #     m[t_step].round(2)
            #     v[t_step].round(2)
            #     breakpoint()
            # noi_adap = noi[t_step] / 3
            wgt_curr += noi_adap

            # ...with attractor weights (learning) --- if learning time.
            t_learning = not t_step % t_present - 1.0  # we start at 1
            if t_learning:
                if verbose:
                    print(f"I'M LEARNING AT ITER {t_step}!")
                # Learning step may need ceiling.
                l_vals = attrac[n_item].copy() * l_r
                wgt_curr += l_vals
                self.weights_str[t_step] += l_vals
                if n_item < n_items - 1:  # XXX
                    n_item += 1  # XXX
                else:  # XXX
                    n_item = 0  # XXX
                if verbose:
                    print(f"Attractor:\n{attrac[n_item]}")
                    print("I WAS learning!")

            if verbose:
                print(f"Weights:\n{wgt_curr}")
                print(f"Neurons:\n{neur_curr}")
            # if t_step == 5:
            #     breakpoint()
            # normalize(wgt_curr, copy=False)  # REMEMBER TO COMMENT OR NOT THIS LINE
            # np.clip(wgt_curr, *mem_min_max, out=wgt_curr)  # FOR MEM MIN MAX
            # np.clip(wgt_curr, -10, 10, out=wgt_curr)  # FOR MEM MIN MAX
            # wgt_curr +=  expit(wgt_curr.copy()) + wgt_curr.copy()  # Inplace.

            # attemp with activations
            # SIGMOID below
            _wgt = wgt_curr.copy()
            wgt_curr -= _wgt
            wgt_curr += 1 / (1 + np.e ** (-_wgt)) * 10 - 5  # (_wgt) - .5)
            # SIGMOID above

            pre_act = wgt_curr @ neur_curr
            if verbose:
                print(f"Preactivations:\n{pre_act}")
            neur_curr = f_act(pre_act)

            if verbose:
                print(f"Neurons after activation:\n{neur_curr}")
                print(f"Target patterns:\n{patts[n_item]}")
            for item in range(n_items):
                loss = f_mse(patts[item], neur_curr)
                # simil = patts[n_item] @ neur[t_step] / len(neur[t_step])
                # print(f"Loss {item}: {loss} (MSE)")
                # print(f"Simil: {simil} (Dot)")
                dict_loss = {"t_step": [t_step], "loss": [loss], "n_pattern": [item]}
                # loss_df = pd.Series(dict_loss)
                loss_df = pd.DataFrame.from_dict(dict_loss)
                loss_df["loss"] = loss
                loss_df["n_pattern"] = item
                self.loss_df = self.loss_df.append(loss_df, ignore_index=True)

        self.loss_df["n_pattern"] = self.loss_df["n_pattern"].astype(int)
        print("Done!")


def net_plot(rate_learning, rate_forgetting, t_presentation, seed=0):
    """Instantiate a network for plotting.

    Parameters
    ----------
    rate_learning : int
    rate_forgetting : int
    t_presentation : int
    seed : int = 0

    Returns
    -------
    net : object

    """

    parameters = Parameters()
    patterns = PatternsGenerator(size=10, num_patterns=1, seed=seed).random_binary()
    net = AttractorNet(parameters, patterns, verbose=False)

    net.parameters.rate_learning = rate_learning
    net.parameters.rate_forgetting = rate_forgetting
    net.parameters.t_presentation = t_presentation
    net.run()

    return net


@cache
def learner_weights():

    fig, axs = plt.subplots(2, 2, sharex=True, sharey=True)

    # Good learner, good schedule.
    net = net_plot(0.8, 0.3, 5.0)
    t_continuous = net.parameters.t_continuous
    t_half = int(t_continuous // 2)
    data = net.weights[t_half]
    sns.heatmap(data, cmap="viridis", vmin=-6, vmax=6, ax=axs[0, 0])
    axs[0, 0].set_title("LG, SG")

    # Good learner, bad schedule.
    net = net_plot(0.8, 0.3, 10.0)
    data = net.weights[t_half]
    sns.heatmap(data, cmap="viridis", vmin=-6, vmax=6, ax=axs[0, 1])
    axs[0, 1].set_title("LG, Sb")

    # Bad learner, good schedule.
    net = net_plot(0.4, 0.7, 5.0)
    data = net.weights[t_half]
    sns.heatmap(data, cmap="viridis", vmin=-6, vmax=6, ax=axs[1, 0])
    axs[1, 0].set_title("Lb, SG")

    # Bad learner, bad schedule.
    net = net_plot(0.4, 0.7, 10.0)
    data = net.weights[t_half]
    sns.heatmap(data, cmap="viridis", vmin=-6, vmax=6, ax=axs[1, 1])
    axs[1, 1].set_title("Lb, Sb")

    plt.tight_layout()
    fig.savefig("figures/conditions_sum_clip.pdf")


def plotty(net):
    # fast fix, won't work
    sns.set_theme()
    sns.set_context("paper")
    plt.close("all")
    fig = sns.relplot(
        x="t_step", y="loss", data=net.loss_df, kind="line", hue="n_pattern"
    )
    # fig.set(xticks=range(30))
    fig.savefig("figures/plotty_sum_clip.pdf")


def similarity(pattern_0, pattern_1):

    p_0 = pattern_0
    p_1 = pattern_1

    assert len(p_0) == len(p_1)
    return p_0 @ p_1


def main() -> None:
    """Main function."""

    parameters = Parameters()
    patterns = PatternsGenerator(size=10, num_patterns=3, seed=1).random_binary()

    # patterns = (1, 1, 0, 0, 0), (0, 0, 0, 1, 1)
    # question = np.array([0, 1, 0, 0, 0])
    # patterns = PatternsGenerator(size=10, num_patterns=3).manual_binary(patterns)

    # patterns = (1, 1, 0, 0, 0), (1, 1, 0, 0, 1), (0, 0, 1, 0, 0)
    # question = 1, 0, 0, 0, 0
    # patterns = PatternsGenerator(size=10, num_patterns=3).manual_binary(patterns)

    # net = AttractorNet(parameters, patterns, verbose=True)
    net = AttractorNet(parameters, patterns, verbose=False)
    net()
    plotty(net)
    learner_weights()
    # plotty(net)
    # print(net.query(question, verbose=True))
    # imshow_slider(net.weights)


if __name__ == "__main__":
    main()

# def forget(self, t_step, f_r, noi):
#     # ...with noise (forgetting).
#     self.weights_str[t_step] = self.weights_str[t_step - 1].copy()
#     res = self.weights_str[t_step].copy()
#     # gaussian activation of noise on resistant positions.
#     res = np.e ** (-0.1 * res ** 2)
#     # forgetting block.
#     noi_adap = noi[t_step].copy() * f_r * res
#     # update model weights with forgetting.
#     self.weights[t_step] += noi_adap

# def learn(self, t_step, l_r):
#     n_items = len(self.patterns)  # XXX
#     # Learning step may need ceiling.
#     l_vals = self.attractors.patterns[self.n_item].copy() * l_r
#     self.weights[t_step] += l_vals
#     self.weights_str[t_step] += l_vals
#     if self.n_item < n_items - 1:  # XXX
#         self.n_item += 1  # XXX
#     else:  # XXX
#         self.n_item = 0  # XXX
