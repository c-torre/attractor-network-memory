import gzip
import math
import pickle
from pathlib import Path

import numpy as np
import requests
import torch
from matplotlib import pyplot

DATA_PATH = Path("data")
PATH = DATA_PATH / "mnist"

PATH.mkdir(parents=True, exist_ok=True)

URL = "https://github.com/pytorch/tutorials/raw/master/_static/"
FILENAME = "mnist.pkl.gz"

if not (PATH / FILENAME).exists():
    content = requests.get(URL + FILENAME).content
    (PATH / FILENAME).open("wb").write(content)


with gzip.open((PATH / FILENAME).as_posix(), "rb") as f:
    ((x_train, y_train), (x_valid, y_valid), _) = pickle.load(f, encoding="latin-1")


x_train, y_train, x_valid, y_valid = map(
    torch.tensor, (x_train, y_train, x_valid, y_valid)
)
n, c = x_train.shape

weights = torch.randn(784, 10) / math.sqrt(784)
weights.requires_grad_()
bias = torch.zeros(10, requires_grad=True)


def log_softmax(x):
    return x - x.exp().sum(-1).log().unsqueeze(-1)


def model(x_batch):
    return log_softmax(x_batch @ weights + bias)


batch_size = 64  # batch size

x_batch = x_train[0:batch_size]  # a mini-batch from x
preds = model(x_batch)  # predictions
preds[0], preds.shape
print(preds[0], preds.shape)


def nll(input, target):
    return -input[range(target.shape[0]), target].mean()


loss_func = nll


def accuracy(out, y_batch):
    preds = torch.argmax(out, dim=1)
    return (preds == y_batch).float().mean()


# mymodel
# params = Parameters()
# patterns = PatternsGenerator(size=10, num_patterns=3, seed=1).random_binary()
# model = AttractorNet(params, patterns, verbose=False)
# net()
# end mymodel

lr = 0.5  # learning rate
epochs = 2  # how many epochs to train for

for epoch in range(epochs):
    print(epoch)
    for i in range((n - 1) // batch_size + 1):
        print(i)
        #         set_trace()
        start_i = i * batch_size
        end_i = start_i + batch_size
        x_batch = x_train[start_i:end_i]
        y_batch = y_train[start_i:end_i]
        y_pred = model(x_batch)
        loss = loss_func(y_pred, y_batch)

        loss.backward()
        with torch.no_grad():
            weights -= weights.grad * lr
            bias -= bias.grad * lr
            weights.grad.zero_()
            bias.grad.zero_()

    print(loss_func(model(x_batch), y_batch), accuracy(model(x_batch), y_batch))
