# attractor-network-memory

Finally an applied attractor network (hopefully)

https://aureliennioche.github.io/LectureUserResearch/

L(obs | theta, H, gamma) =

- ( 1- loss_pattern(obs | theta, H, gamma) ) if recall
- ( loss_pattern(obs | theta, H, gamma) ) if not recall

Parameterization full model: gamma, H, theta
gamma: item parameterization (encoding)
H: history for every subject/user

theta: forgetting rate, noise, ..... for each subject/user
theta = {theta*1, ... , theta_n} with n: number of users
theta_i = {forget_rate_i, noise_i, ...., }
\gamma = \{v*{w, d} \}
with w \in [1, W], with W number of items

d \in [1, D], with D number of dimensions
full model: F
network: N
global loss = mean(L*1, ...., L*{n*obs})
DATA = {obs_1, ...., obs*{n_obs}}
